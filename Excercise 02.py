# ********** EXCERCISE 02 ************

import sys
import os
import platform
import requests
import re
import datetime

# Declare variables
planets = { '1' : ['Mercury', 0.39, 'Merc'], 
            '2' : ['Venus', 0.72, 'Venus'], 
            '3' : ['Earth', 1.00, 'Earth'],
            '4' : ['Mars', 1.52, 'Mars'], 
            '5' : ['Jupiter', 5.20, 'Juptr'], 
            '6' : ['Saturn', 9.58, 'Satrn'], 
            '7' : ['Uranus', 19.20, 'Urnus'], 
            '8' : ['Neptune', 30.05, 'Neptn'], 
            '9' : ['Pluto', 39.48, 'Pluto']}
planetHome = ''
planetWork = ''
planetHelper = ''
flightDate = ''


# Function to Clear Screen
def clearScreen():
    if platform.system() == "Windows":
        os.system('cls')
    else:
        os.system('clear') 

# Function Intro
def intro():
    print('*' * 50)
    print('   Technofy Forecast For Space Travellers   '.center(50, '*'))
    print('*' * 50)

# Function to get planet name
# after input call checkInput to verify
def getPlanetName( location ):
    
    print('Please select your ' + location + ' planet')
    for planetX in list(sorted(planets.keys())):
        print('For *** %s *** type: %s' % ( planets[planetX][0].ljust(8), planetX ) )
    
    choice = input('For *** EXIT     *** please type: exit\n\nSelection: ')
    
    if (choice != 'exit' and choice != 'EXIT'):
        checkInput (choice, location)
    else:
        clearScreen()
        print ('*' *50 + '\n' + '   GoodBye!   '.center(50, '*') + '\n' + '*' *50 + '\n\n')
        sys.exit()

# Function to verify input
def checkInput( planetX, location ):
    global planetHelper
    if planetX in planets:
        planetHelper = planetX
        clearScreen()    
    else:
        clearScreen()
        print ( '"' + planetX + '" is not a valid choice. Please try again!\n')
        getPlanetName ( location )

# Functions to calculate difference between planets and convert to km
def convertAU (dist):
    x = float(dist) * 149598000
    return ( '%.2f km' % x )  

def calcDistance (home, work):
    distance = abs(planets.get(home)[1] - planets.get(work)[1]) 
    return convertAU (distance)    

# Function to get flight date
def getDate():
    global flightDate

    flightDate = input('Please type date when you are travelling in following format YYYY-MM-DD!\nFor *** EXIT     *** please type: exit\n\nDate: ')
    
    if (flightDate != 'exit' and flightDate != 'EXIT'):
        dateReg = re.compile(r'''
                                (\d{4})     # 4 digits for year
                                -           # first separator 
                                ([0-1]\d)   # 2 digits for month, matching 0X or 1X (12 months)
                                -           # second dash
                                ([0-3]\d)   # 2 digits for days, matching 0X or 1X or 2X or 3X (31 days maximum) 
                                ''', re.VERBOSE)    
        # check valid date
        if not dateReg.match(flightDate):
            print ('You have entered the date in wrong format. Please try again! \n')
            getDate() 
    else:
        celearScreen()
        print ('*' *50 + '\n' + '   GoodBye!   '.center(50, '*') + '\n' + '*' *50 + '\n\n')
        sys.exit()

# Function for forcast
def nasaInfo(planet):
    global flightDate
    datePlus7 = str(datetime.datetime.strptime(flightDate, '%Y-%m-%d') + datetime.timedelta(days = 7))
    dateMinus7 = str(datetime.datetime.strptime(flightDate, '%Y-%m-%d') - datetime.timedelta(days = 7))
    
    r = requests.get('https://ssd-api.jpl.nasa.gov/cad.api?date-min=' + dateMinus7 + '&date-max=' + datePlus7 +'&body=' + planets.get(planet)[2] + '&sort=date&fullname=1').json()
    
    if r['count'] != '0': 
        clObj = []

        
        for data in r['data']:
            clObj.append(dict(zip(r['fields'], data)))
        
        print ('\n\n')
        print ('   Forcast for %s   '.center(50, '*') %(planets.get(planet)[0]))
        for i in range(len(clObj)):
            print ('\nObject:'.ljust(15) + '%s ' %(clObj[i]['fullname']) )
            print ('Distance:'.ljust(15) + ' %s ' %( convertAU (clObj[i]['dist']) ))
            print ('Date / Time:'.ljust(15) + '%s ' %(clObj[i]['cd']) )
    else:
        print ('\n\n')
        print ('   Forcast for %s   '.center(50, '*') %(planets.get(planet)[0]))   
        print ('No objects expected\n')

def getNasaData(home, work):
    nasaInfo(home)
    nasaInfo(work)

# Function to show results
def showResults(home, work):
    print ('Home planet: '.ljust(15) + planets.get(home)[0].rjust(15))
    print ('Work planet: '.ljust(15) + planets.get(work)[0].rjust(15))
    print ('Distance: '.ljust(15) + calcDistance(home, work).rjust (15))
    getNasaData(home, work)





# ********************
# SOFTWARE STARTS HERE
# ********************

# Clear screen for cleaner experience
clearScreen ()
# Intro
intro()
# Get Home planet 
getPlanetName ('Home')
planetHome = planetHelper
# Get Work planet
getPlanetName ('Work')
planetWork = planetHelper
# Get travel date
getDate()
#Show distance between planets and forecast
showResults (planetHome, planetWork)






