# ********** EXCERCISE 01 ************
import sys
# Define an Array of Planets and Array of distances from Sun in AZ
# Declare variables
planets = ['Mercury', 'Venus', 'Earth', 'Mars', 'Jupiter', 'Saturn', 'Uranus', 'Neptune', 'Pluto' ]
distances = [0.39, 0.72, 1.00, 1.52, 5.20, 9.58, 19.20, 30.05, 39.48]
planetHelper = 'XYZ' # Array index

# Function to get planet name
# after input calls checkInput to verify
def getPlanetName( location ):
    planet = input('Please type the name of your ' + location + ' planet! \nPlanet: ')
    if (planet != 'exit' and planet != 'EXIT'):
        checkInput (planet, location)
    else:
        sys.exit()

# Function to verify a planet name
# save or call getPlanet if error
def checkInput( planetX, location ):
    global planetHelper
    try:
        planetHelper = planets.index( planetX )
    except ValueError:
        print ( '"' + planetX + '" Doesn\'t exist. Please try again!\n')
        getPlanetName ( location )
   
# Function to calculate difference between planets and convert to km
def calcDistance (home, work):
    x = abs(home - work) * 149598000
    print ('The distance from home to work is: ' + str(x) + 'km')      
 
# Instructions
print('\n*** HR SOFTWARE ***')
print('Instructions: Type "exit" to terminate the program\n\n')
# Prompt + store homeplanet
getPlanetName( 'home' )
# Home planet's distance from Sun
homeplanet = distances[planetHelper]
# Prompt + store homeplanet
getPlanetName( 'work' )
# Home planet's distance from Sun
workplanet = distances[planetHelper]
# Calculate distance and return the value in km
calcDistance ( homeplanet, workplanet )





